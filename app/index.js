const puppeteer = require('puppeteer');
const express = require('express');

const app = express();
const port = 3000;

async function getStreamUrl(email, password) {
    const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
    const page = await browser.newPage();
    
    await page.goto('https://www.elevensports.pt/channel/elevensports1');

    await page.evaluate('javascript: showSignIn();');
    await page.waitFor(2000);
    const frame = await page.frames().find(frame => frame.url().includes('signin'));

    await frame.type('#iptvauth_field_username', email);
    await frame.type('#iptvauth_field_password', password);
    await frame.evaluate('javascript: _auth.processLogin();');
    await page.waitFor(4000);

    // await page.screenshot({path: 'example.png'});
    const m3u8 = await page.$eval('#nlplayerhtml5', el => el.getAttribute('data-src'));

    await browser.close();

    return m3u8;
}

app.get('/elevensports', async (req, res) => {
    const email = req.query.email || '';
    const password = req.query.password || '';

    res.redirect(await getStreamUrl(email, password));
})

app.listen(port, () => console.log(`elevensports-m3u8-auto-login is running.`));
